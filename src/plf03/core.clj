(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (* 3 x))
        z(comp f g)]
    (z 10)))

(defn función-comp-2
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (* 3 x))
        z (comp g f)]
    (z 10)))

(defn función-comp-3
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (* 3 x))
        z (comp f g f g)]
    (z 10)))

(defn función-comp-4
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [xs] (map inc xs))
        z (comp g f)]
    (z #{1 2 4 5 7 8})))

(defn función-comp-5
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [xs] (map inc xs))
        z (comp f g)]
    (z #{1 2 4 5 7 8})))

(defn función-comp-6
  []
  (let [f (fn [x y] (first (range x y)))
        g (fn [x] (str x))
        z (comp g f)]
    (z 3 9)))

(defn función-comp-7
  []
  (let [f (fn [x] (filter even? (range x)))
        z (comp f)]
    (z 15)))

(defn función-comp-8
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [xs] (map dec xs))
        z (comp g f)]
    (z #{1 3 5 6 7 8 9 11 2 13 4})))

(defn función-comp-9
  []
  (let [f (fn [x y] (filter odd? (range x y)))
        g (fn [xs] (map dec xs))
        z (comp g f)]
    (z -3 5)))

(defn función-comp-10
  []
  (let [f (fn [x y] (filter pos? (range x y)))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (map inc xs))
        z (comp h g f)]
    (z -5 10)))

(defn función-comp-11
  []
  (let [f (fn [xs] (filter pos? xs))
       g (fn [xs] (filter even? xs))
        h (fn [xs] (map inc xs))
        z (comp f g h)]
    (z #{-20 10 -3 5 -4 8 4 7 -10 12})))

(defn función-comp-12
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (map inc xs))
        z (comp h g f)]
    (z #{-20 10 -3 5 -4 8 4 7 -10 12})))

(defn función-comp-13
  []
  (let [f (fn [xs] (filter pos-int? xs))
        g (fn [xs] (filter even? xs))
        z (comp g f)]
    (z #{10.2 -20 1.1 30.4 10 -3 5 -4 8 4 7 -10 3.3 12 4.0})))

(defn función-comp-14
  []
  (let [f (fn [xs] (filter string? xs))
        g (fn [xs] (take 1 xs))
        z (comp f g)]
    (z #{"saul" 1 4 "Leon" "gato" 2 3})))

(defn función-comp-15
  []
  (let [f (fn [xs] (* first xs 2))
        g (fn [xs] (+ first xs 3))
        z (comp f g)]
    (z [4 5 6 1 2 7 9])))

(defn función-comp-16
  []
  (let [f (fn [xs] (filter double? xs))
        g (fn [xs] (filter pos? xs))
        h (fn [xs] (filter inc xs))
        z (comp f g h)]
    (z #{10 0.2 -0.5 1.1 4 -1.0 -3 -20.3 -3.3 -2.2})))

(defn función-comp-17
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (dec x))
        h (fn [x] (* x x))
        z (comp f g h)]
    (z 5)))

(defn función-comp-18 
  []
  (let [f (fn [xs] (filter number? xs))
        g (fn [xs] (filter pos?) xs)
        z (comp f g)]
    (z #{"s" \s 1 3 4 1.0 "g"  "d" 12 45 40 30 14 1.9 3.4 5.5})))

(defn función-comp-19
  []
  (let [f (fn [xs] (filter number? xs))
        g (fn [xs] (filter pos?) xs)
        h (fn [xs] (filter double? xs))
        z (comp f g h)]
    (z #{"s" \s 1 3 4 1.0 "g"  "d" 12 45 40 30 14 1.9 3.4 5.5})))

(defn función-comp-20
  []
  (let [f (fn [xs] (filter number? xs))
        g (fn [xs] (filter pos?) xs)
        h (fn [xs] (filter integer? xs))
        z (comp g h f)]
    (z #{"s" \s 1 3 4 1.0 "g"  "d" 12 45 40 30 14 1.9 3.4 5.5})))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)


(defn función-complement-1
  []
  (let [g (fn [x] (even? x))
        z (complement g)]
    (z 10)))

(defn función-complement-2
  []
  (let [f (fn [y] (even? y))
        z (complement f)]
    (z 13)))

(defn función-complement-3
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 13)))

(defn función-complement-4
  []
  (let [f (fn [xs] (pos? (first xs)))
        z (complement f)]
    (z #{-2 3 4})))

(defn función-complement-5
  []
  (let [g (fn [xs] (map inc xs))
        z (complement g)]
    (z #{1 2 3 4})))

(defn función-complement-6
  []
  (let [f (fn [xs] (neg-int? (first xs)))
        z (complement f)]
    (z [1.0 2 3 5])))

(defn función-complement-7
  []
  (let [f (fn [x] (pos? (first (range x))))
        z (complement f)]
    (z 10)))

(defn función-complement-8
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z "saul")))

(defn función-complement-9
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z 10)))

(defn función-complement-10
  []
  (let [f (fn [y] (char? y))
        z (complement f)]
    (z 'd')))

(defn función-complement-11
  []
  (let [f (fn [xs] (char? (take-last 1 xs)))
        z (complement f)]
    (z #{'c' "saul" "marcos" 3 2 5})))

(defn función-complement-12
  []
  (let [f (fn [xs] (double? (first xs)))
        z (complement f)]
    (z [2.3 'c' "saul" "marcos" 3 2 5.0])))

(defn función-complement-13
  []
  (let [f (fn [x y] (pos-int? (first (range x y))))
        z (complement f)]
    (z -3 3)))

(defn función-complement-14
  []
  (let [f (fn [x] (true? x))
        z (complement f)]
    (z true)))

(defn función-complement-15
  []
  (let [f (fn [xs bx] (even? (+ (first xs) (first bx))))
        z (complement f)]
    (z #{4 9 1} #{6 4 5})))

(defn función-complement-16
  []
  (let [f (fn [sx bx] (integer? (+ (apply min sx) (apply min bx))))
        z (complement f)]
    (z #{3 5 7 2} #{9 3 4})))

(defn función-complement-17
  []
  (let [f (fn [xs bx] (number? (last (map comp xs bx))))
        z (complement f)]
    (z #{1 4 'h' "s" \a 3 "g" 'p'} #{2 40 10 20 "j" "r" 'f' \k})))

(defn función-complement-18
  []
  (let [f (fn [xs] (hash-set even? xs))
        z (complement f)]
    (z #{1 2 3 4})))

(defn función-complement-19
  []
  (let [f (fn [xs] (empty? xs))
        z (complement f)]
    (z [])))

(defn función-complement-20
  []
  (let [f (fn [xs] (empty? (first xs)))
        z (complement f)]
    (z #{#{2 3 4} #{5 6 7}})))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

(defn función-constantly-1
  []
  (let [argumento-orignal true
        z (constantly  argumento-orignal)]
    (z 10)))

(defn función-constantly-2
  []
  (let [argumento [1 2 3 4 5]
        z (constantly argumento)]
    (z #{3 4 5})))

(defn función-constantly-3
  []
  (let [f (fn [y] (filter even? y))
        z (constantly f)]
    (z #{3 4 5})))

(defn función-constantly-4
  []
  (let [argumento (first [10 20])
        z (constantly argumento)]
    (z 10 30 40)))

(defn función-constantly-5
  []
  (let [argumento "saul"
        z (constantly argumento)]
    (z "sergio")))

(defn función-constantly-6
  []
  (let [argumento (string? "saul")
        z (constantly argumento)]
    (z nil)))

(defn función-constantly-7
  []
  (let [argumento (range -3 4)
        z (constantly argumento)]
    (z 3)))

(defn función-constantly-8
  []
  (let [argumento (take 4 (range -4 11 2))
        z (constantly argumento)]
    (z #{2 3 "E" 'H'})))

(defn función-constantly-9
  []
  (let [argumento #{'a' 'b' 'c' \b 2}
        z (constantly argumento)]
    (z 1000)))

(defn función-constantly-10
  []
  (let [argumento (* (first [4 10]) (first [1.3 2.4 0.6]))
        z (constantly argumento)]
    (z "Hola mundo")))

(defn función-constantly-11
  []
  (let [argumento {:a 4 :b 10 :d 20}
       z (constantly argumento)]
    (z 1000)))

(defn función-constantly-12
  []
  (let [argumento (map even? #{1 2 3 4 5 6 7 8 9 10 11 12 13 15 16 18})
       z (constantly argumento)]
    (z 1000)))

(defn función-constantly-13
  []
  (let [f (fn [x] (inc x))
        h (constantly (f 5))]
    (h 10)))

(defn función-constantly-14
  []
  (let [g (fn [f] (* f 3))
        h (constantly g)]
    ((h 10) 100)))

(defn función-constantly-15
  []
  (let [argumento '(1 2 3 4 5 6 9 10)
        z (constantly argumento)]
    (z [10 20 30 4])))

(defn función-constantly-16
  []
  (let [argumento (filter double? #{true 10.1 30 1.4 3 3.33 5 1.10 4.4 false 2 4 0.9})
        z (constantly argumento)]
    (z )))

(defn función-constantly-17
  []
  (let [argumento (filter even? #{1 50 25 24 60 66 35 44 22 28 30 2 3 4 5 6 7 8 9 10 11 12 32 19 18 200 300 150})
        z (constantly argumento)]
    (z (first [1 2 "s"]))))

(defn función-constantly-18
  []
  (let [argumento (filter string? #{3 4 5 "sergio" 10.1 true false 'f' "saul" 20 't' "ramirez" 40 "lopez" 3.33})
                   z (constantly argumento)]
    (z 20 30 10)))

(defn función-constantly-19
  []
  (let [argumento (filter symbol? #{3 4 5 "sergio" 10.1 true false 'f' "saul" 20 't' "ramirez" 40 "lopez" 3.33})
        z (constantly argumento)]
    (z nil?)))

(defn función-constantly-20
  []
  (let [argumento (filter coll? #{#{1 2 3 4} false "f" [true false] '("a" "b" "c")})
        z (constantly argumento)]
    (z nil?)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [f (fn [x] odd? x)
        g (fn [x] pos? x)
        z (every-pred f g)]
    (z 3)))

(defn función-every-pred-2
  []
  (let [f (fn [xs] pos? xs)
        g (fn [xs] ratio? xs)
        z (every-pred f g)]
    (z [0 2/3 -2/3 1/4 -1/10 5 3/3])))

(defn función-every-pred-3
  []
  (let [f (fn [xs] (pos? (first xs)))
        g (fn [xs] (constantly xs))
        z (every-pred f g)]
    (z #{-2 3 4})))

(defn función-every-pred-4
  []
  (let [f (fn [xs] (filter pos-int? xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (map inc xs))
        z (every-pred f g h)]
    (z #{-2 -4 -6 2 4 5 3 7 8 9 10 11 21 24 55 22})))

(defn función-every-pred-5
  []
  (let [f (fn [xs] (filter pos-int? xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (filter inc xs))
        z (every-pred f g h)]
    (z #{-2 -4 -6 2 4 22})))


(defn función-every-pred-6
  []
  (let [f (fn [xs] (max xs))
        g (fn [xs] (min xs))
        z (every-pred g f)]
    (z #{1 3 5 6 79 10})))

(defn función-every-pred-7
  []
  (let [f (fn [x] (associative? x))
        z (every-pred f)]
    (z 10)))

(defn función-every-pred-8
  []
  (let [f (fn [x] (associative? x))
        z (every-pred f)]
    (z {:a 1 :b 2 :c 3 :d 4})))

(defn función-every-pred-9
  []
  (let [f (fn [xs] (pos? xs))
        g (fn [xs] (ratio? xs))
        z (filter (every-pred f g))]
    (z [0 2/3 -2/3 1/4 -1/10 5 3/3])))

(defn función-every-pred-10
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (pos? x))
        h (fn [x] (double? x))
        i (fn [x] (decimal? x))
        j (fn [x] (integer? x))
        z (every-pred f g h i j)]
    (z (take-last 1 (range -3 4)))))

(defn función-every-pred-11
  []
  (let [f (fn [x] (int? (first (range x))))
        z (every-pred f)]
    (z 3)))

(defn función-every-pred-12
  []
  (let [f (fn [xs] (filter string? xs))
        z (every-pred f)]
    (z #{"Hola" "mundo"})))

(defn función-every-pred-13
  []
  (let [f (fn [xs] (neg? (last xs)))
        g (fn [xs] (constantly xs))
        z (every-pred f g)]
    (z #{-2 3 4 10 -10})))

(defn función-every-pred-14
  []
  (let [f (fn [xs] (neg? (last xs)))
        g (fn [xs] (constantly xs))
        z (every-pred f g)]
    (z #{-2 3 4 10 20})))

(defn función-every-pred-15
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (pos? x))
        h (fn [x] (double? x))
        i (fn [x] (decimal? x))
        z (every-pred f g h i )]
    (z (take 4 (range -3 9)))))

(defn función-every-pred-16
  []
  (let [f (fn [x] (associative? x))
        z (every-pred f)]
    (z [1 3 5 6 7 8])))

(defn función-every-pred-17
  []
  (let [f (fn [xs] (filter even? xs))
        z (every-pred f)]
    (z #{1 3 5 6 7 8 9 10 11 14 16})))

(defn función-every-pred-18
  []
  (let [f (fn [xs] (reversible? xs))
        z (every-pred f)]
    (z #{1 3 5})))

(defn función-every-pred-19
  []
  (let [f (fn [xs] (reversible? xs))
        g (fn [xs] (reversible? xs))
        z (every-pred f g)]
    (z #{1 3 5} #{})))

(defn función-every-pred-20
  []
  (let [f (fn [xs] (map? (first xs)))
        g (fn [xs] (constantly xs))
        z (every-pred f g)]
    (z #{#{2 3 4} [5 6 7] '("g" "f" "h")})))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-partial-1
  []
  (let [f (fn [x y z] (hash-set x y z))
        z (partial f 10)]
    (z 20 30)))

(defn función-partial-2
  []
  (let [f (fn [xs y] (hash-map xs y))
        z (partial f 10)]
    (z #{40 50 60 88})))

(defn función-partial-3
  []
  (let [f (fn [xs a b c] (hash-set (xs (range a b c))))
        z (partial f #{10 20 12 15})]
    (z -10 11 2)))

(defn función-partial-4
  []
  (let [f (fn [x y] (+ x y))
        z (partial f)]
    (z 1 1)))

(defn función-partial-5
  []
  (let [f (fn [xs bx] (conj xs bx))
        z (partial f)]
    (z [0 1] [2 3 4 5])))

(defn función-partial-6
  []
  (let [f (fn [xs a b] (disj xs a b))
        z (partial f)]
    (z #{-3 -2 -1 0 1 2 3 4} 1 3)))

(defn función-partial-7
  []
  (let [f (fn [xs a] (hash-map xs a))
        z (partial f)]
    (z #{-3 -2 -1 0 1 2 3 4} 1)))

(defn función-partial-8
  []
  (let [f (fn [xs a b] (hash-set xs (range a b)))
        z (partial f)]
    (z #{-3 -2 -1 0 1 2 3 4} -5 6)))

(defn función-partial-9
  []
  (let [f (fn [xs] (filter even? xs))
        z (partial f)]
    (z (range -3 5))))

(defn función-partial-10
  []
  (let [f (fn [x y] (* x y))
        z (partial f)]
    (z 4 7)))

(defn función-partial-11
  []
  (let [f (fn [x y] (* (first x) (first y)))
        z (partial f)]
    (z (range 3 5) (range -2 7))))

(defn función-partial-12
  []
  (let [f (fn [a b c d] (hash-set (range a b) (range c d)))
        z (partial f)]
    (z -3 4 -5 6)))

(defn función-partial-13
  []
  (let [f (fn [a b c d] (conj (range a b) (range c d)))
        z (partial f)]
    (z -3 4 -5 6)))

(defn función-partial-14
  []
  (let [f (fn [xs c d] (conj xs c d))
        z (partial f)]
    (z (range -3 4) -5 6)))

(defn función-partial-15
  []
  (let [f (fn [xs] (filter even? xs) (filter pos-int? xs))
        z (partial f)]
    (z (range -3 4))))

(defn función-partial-16
  []
  (let [f (fn [xs] (hash-set (filter string? xs) (filter char? xs)))
        z (partial f)]
    (z #{"saul" 'd' \j 10 'h' 'k' "sergio" 2 3 4 "Hola" 'y'})))

(defn función-partial-17
  []
  (let [f (fn [a b c d e f] (hash-map a b c d e f))
        z (partial f)]
    (z :a :b :c 1 2 3)))

(defn función-partial-18
  []
  (let [f (fn [a b] (conj a b))
        z (partial f)]
    (z (range -3 10 3) (range -4 11 2))))

(defn función-partial-19
  []
  (let [f (fn [xs] (hash-set (filter string? xs)))
        z (partial f)]
    (z #{"hola" 'h' 3 1 5 4 6 "Tecnologico" 2 \c "buen dia" 0})))

(defn función-partial-20
  []
  (let [f (fn [xs] (hash-set (filter odd? xs) (filter inc xs)))
        z (partial f)]
    (z (range -3 5))))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

(defn función-juxt-1
  []
  (let [f (fn [a] (first a))
        g (fn [a] (count a))
        z (juxt f g)]
    (z "Clojure Rocks")))

(defn función-juxt-2
  []
  (let [f (fn [xs] (first xs))
        g (fn [a] (string? a))
        z (juxt f g)]
    (z "saul")))

(defn función-juxt-3
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (take-last 1 xs))
        h (fn [xs] (number? xs))
        z (juxt f g h)]
    (z #{#{1 2 3 4} '(9 4 5)})))

(defn función-juxt-4
  []
  (let [f (fn [xs] (take 1 xs))
        g (fn [xs] (char? xs))
        z (juxt f g)]
    (z #{'s' 2 'd' 4 "d"})))

(defn función-juxt-5
  []
  (let [f (fn [xs] (take 1 xs))
        g (fn [xs] (char? xs))
        h (fn [xs] (count xs))
        z (juxt f g h)]
    (z #{'s' 2 'd' 4 "d"})))

(defn función-juxt-6
  []
  (let [f (fn [xs] (take-last 1 xs))
        g (fn [xs] (char? xs))
        h (fn [xs] (count xs))
        i (fn [xs] (first xs))
        z (juxt f g h i)]
    (z "Tecnologico Nacional de Mexico")))

(defn función-juxt-7
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (identity xs))
        h (fn [xs] (count xs))
        z (juxt f g h)]
    (z #{'s' 2 'd' 4 "d"})))

(defn función-juxt-8
  []
  (let [f (fn [xs] (min xs))
        g (fn [xs] (max xs))
        h (fn [xs] (count xs))
        z (juxt f g h)]
    (z '(1 2 3 4 5 6))))

(defn función-juxt-9
  []
  (let [f (fn [xs] (apply min xs))
        g (fn [xs] (apply max xs))
        h (fn [xs] (count xs))
        z (juxt f g h)]
    (z '(1 2 3 4 5 6))))

(defn función-juxt-10
  []
  (let [f (fn [x y] (range x y))
        g (fn [x y] (+ x y))
        z (juxt f g)]
    (z -1 5)))

(defn función-juxt-11
  []
  (let [f (fn [x y] (range x y))
        g (fn [x y] (+ x y))
        z (juxt g f)]
    (z -1 5)))

(defn función-juxt-12
  []
  (let [f (fn [x y] (range x y))
        g (fn [x y] (hash-set (first (range x y)) (last (range x y))))
        z (juxt f g)]
    (z -1 5)))

(defn función-juxt-13
  []
  (let [f (fn [x y] (first (range x y)))
        g (fn [x y] (hash-set (first (range x y)) (last (range x y))))
        z (juxt f g)]
    (z -3 6)))

(defn función-juxt-14
  []
  (let [f (fn [xs] (take 1 xs))
        g (fn [xs] (take 12 xs))
        h (fn [xs] (take 24 xs))
        z (juxt h g f)]
    (z "Tecnologico Nacional de Mexico")))

(defn función-juxt-15
  []
  (let [f (fn [xs] (take-while int? xs))
        g (fn [xs] (take-while neg? xs))
        z (juxt g f)]
    (z [-2 -1  0 4 -6 -7 1 2 3 5])))

(defn función-juxt-16
  []
  (let [f (fn [x y] (range x y))
        g (fn [x y] (filter even? (range x y)))
        z (juxt f g)]
    (z -5 11)))

(defn función-juxt-17
  []
  (let [f (fn [xs] (filter coll? xs))
        z (juxt f)]
    (z [[] (sorted-set) [4 5] '() #{} (sorted-map)])))

(defn función-juxt-18
  []
  (let [f (fn [xs] (keys xs))
        g (fn [xs] (vals xs))
        z (juxt f g)]
    (z {:a 1 :b 2 :c 4 :f 6})))

(defn función-juxt-19
  []
  (let [f (fn [xs] (keys xs))
        g (fn [xs] (vals xs))
        h (fn [xs] (count xs))
        z (juxt f g h)]
    (z {:a 1 :b 2 :c 4 :f 6})))

(defn función-juxt-20
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (last xs))
        i (fn [xs] (count xs))
        z (juxt f g i)]
    (z "Oaxaca")))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

(defn función-fnil-1
  []
  (let [f (fn [x y] (if(> x y) (+ x y) (- x y)))
        z (fnil f 7)]
    (z nil 6)))

(defn función-fnil-2
  []
  (let [f (fn [xs bx] (if (> (first xs) (first bx)) (+ (first xs) (last xs)) (- (first xs) (last xs))))
        z (fnil f [2 3])]
    (z nil [2 4])))

(defn función-fnil-3
  []
  (let [f (fn [x] (nat-int? x))
        z (fnil f nil)]
    (z 10)))

(defn función-fnil-4
  []
  (let [f (fn [x] (nat-int? x))
        z (fnil f 10)]
    (z nil)))

(defn función-fnil-5
  []
  (let [f (fn [x] (if(number? x) (inc x) (str "No es numero")))
        z (fnil f "e")]
    (z nil)))

(defn función-fnil-6
  []
  (let [f (fn [xs] (if (string? (first xs)) (juxt (first xs) (apply count xs)) (last xs)))
        z (fnil f nil)]
    (z "Sergio")))

(defn función-fnil-7
  []
  (let [f (fn [xs] (if(coll? xs) (+ (first xs) (last xs)) (str "El dato no es una colección")))
       z (fnil f [3 4 5 6 7])]
    (z nil)))

(defn función-fnil-8
  []
  (let [f (fn [a b] (if(< a b) (str "el mayor es:" b) (str "el mayor es:" a)))
        z (fnil f 8)]
    (z nil 10)))

(defn función-fnil-9
  []
  (let [f (fn [a b] (if (< a b) (str "el mayor es:" b) (str "el mayor es:" a)))
        z (fnil f nil)]
    (z 20 16)))

(defn función-fnil-10
  []
  (let [f (fn [x] (coll? x))
        z (fnil f nil)]
    (z {:x 5})))

(defn función-fnil-11
  []
  (let [f (fn [x y] (if(number? x) (if(number? y) (+ x y) (str y "no es numero")) (str y "no es numero")))
       z (fnil f 10)]
    (z nil 20)))

(defn función-fnil-12
  []
  (let [f (fn [xs] (if (string? (first xs)) (juxt (first xs) (apply count xs)) (last xs)))
        z (fnil f "hola")]
    (z nil)))

(defn función-fnil-13
  []
  (let [f (fn [x y] (if (number? x) (if (number? y) (+ x y) (str y "no es numero")) (str y "no es numero")))
        z (fnil f 10)]
    (z nil "fau")))

(defn función-fnil-14
  []
  (let [f (fn [x] (if (number? x) (inc x) (str "No es numero")))
        z (fnil f nil)]
    (z (last (range 4)))))

(defn función-fnil-15
  []
  (let [f (fn [x y] (if (> x y) (inc x) (dec y)))
        z (fnil f (first (range -1 5)))]
    (z nil (last (range 3)))))

(defn función-fnil-16
  []
  (let [f (fn [x y] (if (char? x) (if (char? y) (str x y) (str y "no es character")) (str x "no es character")))
        z (fnil f 20)]
    (z nil 's')))

(defn función-fnil-17
  []
  (let [f (fn [x y] (if (char? x) (if (char? y) (str x y) (str y "no es character")) (str x "no es character")))
        z (fnil f 10)]
    (z nil 3)))

(defn función-fnil-18
  []
   (let [f (fn [x z] (* x z))
         z (fnil f 2)]
     (z nil 20)))

(defn función-fnil-19
  []
  (let [f (fn [x] (if (pos-int? x) (inc x) (dec x)))
        z (fnil f nil)]
    (z (last (range -3 5)))))

(defn función-fnil-20
  []
   (let [f (fn [x] (inc (inc x)))
        z (fnil f nil)]
    (z 5)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)


(defn función-some-fn-1
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (zero? x))
        h (fn [x] (string? x))
        z (some-fn f g h)]
    (z 10)))

(defn función-some-fn-2
  []
  (let [f (fn [xs] (filter number? xs))
        z (some-fn f)]
    (z #{1 2 3 4 5 6})))

(defn función-some-fn-3
  []
  (let [f (fn [xs] (filter number? xs))
        z (some-fn f)]
    (z #{2 4 "f" 'f' 'h' 3 7 3.3 6 1.1 0 5 "d" \f})))

(defn función-some-fn-4
  []
  (let [f (fn [xs] (filter number? xs))
        h (fn [xs] (filter double? xs))
        z (some-fn h f)]
    (z #{2 4 "f" 'f' 'h' 3 7 3.3 6 1.1 0 5 "d" \f})))

(defn función-some-fn-5
  []
  (let [f (fn [xs] (filter number? xs))
        g (fn [xs] (filter even? xs))
        z (some-fn f g)]
    (z (range -3 5))))

(defn función-some-fn-6
  []
  (let [f (fn [xs] (filter number? xs))
        g (fn [xs] (filter even? xs))
        z (some-fn g f)]
    (z (range -3 5))))

(defn función-some-fn-7
  []
  (let [f (fn [xs] (filter number? xs))
        g (fn [xs] (filter odd? xs))
        h (fn [xs] (rest xs))
        z (some-fn g f h)]
    (z (range -9 10))))

(defn función-some-fn-8
  []
  (let [f (fn [xs] (range (first xs) (last xs)))
        g (fn [xs] (filter number? xs))
        h (fn [xs] (filter odd? xs))
        z (some-fn h g f)]
    (z [-3 5 10 20])))

(defn función-some-fn-9
  []
  (let [f (fn [xs] (vector? xs))
        h (fn [xs] (first boolean? xs))
        z (some-fn f h)]
    (z [true 2 3 false])))

(defn función-some-fn-10
  []
  (let [f (fn [xs] (vector? xs))
        h (fn [xs] (filter boolean? xs))
        z (some-fn f h)]
    (z [1 true 2 3 false])))

(defn función-some-fn-11
  []
  (let [f (fn [xs] (filter number? xs))
        i (fn [xs] (filter int? xs))
        j (fn [xs] (filter even? xs))
        z (some-fn j i f)]
    (z (range -10 11))))

(defn función-some-fn-12
  []
  (let [f (fn [xs] (string? xs))
        z (some-fn f)]
    (z "saul")))

(defn función-some-fn-13
  []
  (let [f (fn [xs] (filter string? (first xs)))
        h (fn [xs] (coll? (first xs)))
        z (some-fn f h)]
    (z [["coca" 4 5 "cola" 'g'] [1 2 3 4] ['a' 'b' 'c']])))

(defn función-some-fn-14
  []
  (let [f (fn [xs] (filter string? (first xs)))
        h (fn [xs] (coll? (first xs)))
        z (some-fn (juxt f h))]
    (z [["coca" 4 5 "cola" 'g'] [1 2 3 4] ['a' 'b' 'c']])))


(defn función-some-fn-15
  []
  (let [f (fn [xs] (filter number? xs))
        h (fn [xs] (map char xs))
        z (some-fn (juxt f h))]
    (z (range 65 69))))

(defn función-some-fn-16
  []
  (let [f (fn [xs] (filter number? xs))
        h (fn [xs] (map char xs))
        i (fn [xs] (apply str xs))
        z (some-fn (juxt f h i))]
    (z [67 108 111 106 117 114 101])))

(defn función-some-fn-17
  []
  (let [f (fn [xs] (filter number? xs))
        h (fn [xs] (map char xs))
        i (fn [xs] (apply str (map char xs)))
        z (some-fn (juxt f h i))]
    (z [67 108 111 106 117 114 101])))


(defn función-some-fn-18
  []
  (let [f (fn [xs] (filter string? xs))
        g (fn [xs] (apply str (reverse xs)))
        z (some-fn (juxt f g))]
    (z "food")))

(defn función-some-fn-19
  []
  (let [f (fn [xs] (filter number? xs))
        h (fn [xs] (map char xs))
        i (fn [xs] (apply str (map char xs)))
        j (fn [xs] (apply str (reverse (map char xs))))
        z (some-fn (juxt j i h f))]
    (z [67 108 111 106 117 114 101])))

(defn función-some-fn-20
  []
  (let [f (fn [xs] (filter number? xs))
        g (fn [xs] (filter pos? xs))
        i (fn [xs] (filter pos? ( filter int? xs)))
        j (fn [xs] (filter even? (filter pos?(filter int? xs))))
        z (some-fn (juxt f g i j))]
    (z (range -10 11))))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)